# The easy way to Powershell

[**Deutsch**](https://gitlab.com/hacklabor/the-easy-way-to-powershell/-/blob/main/README.md),
[**English**](https://gitlab.com/hacklabor/the-easy-way-to-powershell/-/blob/main/README.en.md)


## What is PowerShell?

> PowerShell is a task automation and configuration management program from Microsoft, consisting of a command-line shell and the associated scripting language. Initially a Windows component only, known as Windows PowerShell, it was made open-source and cross-platform on 18 August 2016 with the introduction of PowerShell Core. The former is built on the .NET Framework, the latter on .NET (previously .NET Core). 

<sub>_Page „PowerShell“. In: Wikipedia – Wikipedia, the free encyclopedia. Last edited on 5 February 2023, at 16:19 (UTC, 17:25 UTC. URL: https://en.wikipedia.org/w/index.php?title=PowerShell&oldid=1137616898 (Retrieved: 10 February 2023, 20:29 UTC)_</sub>

PowerShell is a useful tool that is installed by default on every modern version of Windows. PowerShell Core can be installed on all common operating systems.


## Anhang

### Support
Questions, suggestions and error messages are welcome via ticket. Pull requests are welcome..

### Roadmap

- Define the structure of the guide
  - Distribute the guide over several sub-files
- Create directory structure for code examples

### Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

### Authors and acknowledgment

- [Stefan Hausmann](https://gitlab.com/daemon_necis)
- [Deumus](https://gitlab.com/Deumus)
- [Michael Gauer](https://gitlab.com/Brain09)

### License
For open source projects, say how it is licensed.

### Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
