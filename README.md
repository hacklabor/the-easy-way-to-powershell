# Der einfache Weg zu PowerShell

[**Deutsch**](https://gitlab.com/hacklabor/the-easy-way-to-powershell/-/blob/main/README.md),
[**English**](https://gitlab.com/hacklabor/the-easy-way-to-powershell/-/blob/main/README.en.md)


## Was ist PowerShell?

> PowerShell (auch Windows PowerShell und PowerShell Core) ist ein plattformübergreifendes Framework von Microsoft zur Automatisierung, Konfiguration und Verwaltung von Systemen, das einen Kommandozeileninterpreter inklusive Skriptsprache bietet.<br />Windows PowerShell basiert auf der Common Language Runtime (CLR) des .NET Frameworks und wird mit Windows als Teil des Windows Management Frameworks (WMF) unter einer proprietären Lizenz ausgeliefert.Seit 2016 gibt es auch die Windows PowerShell als Core Edition, welche wie PowerShell Core auf .NET Core basiert und als Teil von Windows Nano Server und Windows IoT ausgeliefert wird.<br />Seit Version 6 basiert PowerShell auf der Core Common Language Runtime (CoreCLR) und ist als plattformübergreifendes Open-Source-Projekt unter der MIT-Lizenz für Linux, macOS und Windows verfügbar.

<sub>_Seite „PowerShell“. In: Wikipedia – Die freie Enzyklopädie. Bearbeitungsstand: 5. Januar 2023, 17:25 UTC. URL: https://de.wikipedia.org/w/index.php?title=PowerShell&oldid=229522209 (Abgerufen: 10. Februar 2023, 20:29 UTC)_</sub>

PowerShell ist ein nützliches Wekzeug, dass bei jeder modernen Windowsversion standardmäßig installiert ist. Auf allen gängigen Betriebssystemen kann man PowerShell Core installieren.


## Anhang

### Support
Fragen, Anregungen und Fehlermeldungen gerne per Ticket. Pull-Request sind willkommen.

### Roadmap

- Struktur des Guides festlegen
  - Guide auf mehrere Unterdateien verteilen
- Verzeichnisstruktur für Codebeispiele anlegen

### Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

### Authors and acknowledgment

- [Stefan Hausmann](https://gitlab.com/daemon_necis)
- [Deumus](https://gitlab.com/Deumus)
- [Michael Gauer](https://gitlab.com/Brain09)

### License
For open source projects, say how it is licensed.

### Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
